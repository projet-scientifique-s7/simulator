import model.Coord;
import model.Fire;
import model.Sensor;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class SimulatorManager {
    //Attributes
    public List<Sensor> sensors = new ArrayList<Sensor>();
    public List<Fire> fires = new ArrayList<Fire>();
    public static final int LIMIT_COORD = 500;
    private static final int NB_ZONES_EXTENSION = 4;
    public Map<Integer, Integer> zones = new HashMap<Integer, Integer>();

    public SimulatorManager(){
        createZones();
    }

    //functions
    private void createZones(){
        zones.put(1, (int) (0.02*LIMIT_COORD));
        zones.put(2, (int) (0.03*LIMIT_COORD));
        zones.put(3, (int) (0.05*LIMIT_COORD));
        zones.put(4, (int) (0.08*LIMIT_COORD));
        zones.put(5, (int) (0.10*LIMIT_COORD));
        zones.put(6, (int) (0.13*LIMIT_COORD));
        zones.put(7, (int) (0.16*LIMIT_COORD));
        zones.put(8, (int) (0.20*LIMIT_COORD));
        zones.put(9, (int) (0.25*LIMIT_COORD));
        zones.put(10, (int) (0.35*LIMIT_COORD));
    }

    private Fire getFireBySensor(Sensor sensor){
        for(int i = 0; i < fires.size(); i++){
            if(fires.get(i).sensors.contains(sensor)) return fires.get(i);
        }
        return null;
    }

    public void simulateFire(Coord coords) throws InterruptedException {
        Sensor sensor = getNearestSensor(coords);
        sensor.setState(new Random().nextInt(10 + 5) + 5);
        fires.add(new Fire(sensor));
        sensor.display();
        //TODO Dans un thread
        TimeUnit.SECONDS.sleep(10);
        fireEnlargement(sensor, NB_ZONES_EXTENSION);
    }

    public Sensor getNearestSensor(Coord coords){
        Sensor sensor = new Sensor("-1", 0, 0);
        float tmpLength = LIMIT_COORD*2;
        for(int i = 0; i < sensors.size(); i++){
            float diffX = sensors.get(i).coordinates.x - coords.x;
            float diffY = sensors.get(i).coordinates.y - coords.y;
            float length = (float) Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
            if(length < tmpLength) {
                tmpLength = length;
                sensor = sensors.get(i);
            }
        }
        return sensor;
    }

    public Sensor getSensorNearestOther(Sensor _sensor){
        Sensor sensor = new Sensor("-1", 0, 0);
        float tmpLength = LIMIT_COORD*2;
        for(int i = 0; i < sensors.size(); i++){
            if(_sensor.id == sensors.get(i).id){
                continue;
            }
            float diffX = sensors.get(i).coordinates.x - _sensor.coordinates.x;
            float diffY = sensors.get(i).coordinates.y - _sensor.coordinates.y;
            float length = (float) Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
            if(length < tmpLength) {
                tmpLength = length;
                sensor = sensors.get(i);
            }
        }
        return sensor;
    }

    public void fireEnlargement(Sensor epicenter, int nbZonesExtension) {
        //regarder le sensor épicentre et élargir le feu selon la zone
        Fire fire = getFireBySensor(epicenter);
        for (int i = 0; i < nbZonesExtension; i++) {
            //TODO threads of it with func above and test it
        }
    }

    public void activateSensorsInZone(Fire fire, Coord coords, int zone, int state){
        //Entre le state du sensor parent et -3
        for(int i = 0; i < sensors.size(); i++){
            if(!isOnZone(sensors.get(i), coords, zone)) continue;
            sensors.get(i).setState(new Random().nextInt(state + (state-3)));
            fire.sensors.add(sensors.get(i));
        }
    }

    public boolean isOnZone(Sensor sensor, Coord coords, int _zone){
        int zone = zones.get(_zone);
        if((sensor.coordinates.x <= zone + coords.x && sensor.coordinates.x >= zone - coords.x)
                && (sensor.coordinates.y <= zone + coords.y && sensor.coordinates.y >= zone - coords.y)){
            return true;
        }
        return false;
    }

    //Debug
    public void displayList(){
        for(int i = 0; i < sensors.size(); i++){
            sensors.get(i).display();
        }
        System.out.println("");
    }
}
