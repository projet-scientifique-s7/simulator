package Utils;

import model.APIObject;
import model.Coord;
import model.Sensor;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Request {

    URL url;
    HttpURLConnection httpURLConnection;

    public Request(String url) throws IOException {
        this.url = new URL(url);
        this.httpURLConnection = (HttpURLConnection)this.url.openConnection();
    }

    public void POSTApiObject(APIObject apiObject) throws IOException {

        this.httpURLConnection.setRequestMethod("POST");

        this.httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
        this.httpURLConnection.setRequestProperty("Accept", "application/json");

        this.httpURLConnection.setDoOutput(true);

        String jsonInputString = apiObject.getJSONString();
        System.out.println(jsonInputString);

        try(OutputStream os = this.httpURLConnection.getOutputStream()){
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int code = this.httpURLConnection.getResponseCode();
        System.out.println(code);

        try(BufferedReader br = new BufferedReader(new InputStreamReader(this.httpURLConnection.getInputStream(), "utf-8"))){
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }
    }

    public void PUTApiObject(APIObject apiObject) throws IOException {

        this.httpURLConnection.setRequestMethod("PUT");

        this.httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
        this.httpURLConnection.setRequestProperty("Accept", "application/json");

        this.httpURLConnection.setDoOutput(true);

        String jsonInputString = apiObject.getJSONStringToPUT();
        System.out.println(jsonInputString);

        try(OutputStream os = this.httpURLConnection.getOutputStream()){
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        int code = this.httpURLConnection.getResponseCode();
        System.out.println(code);

        try(BufferedReader br = new BufferedReader(new InputStreamReader(this.httpURLConnection.getInputStream(), "utf-8"))){
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }
    }

    public JSONArray GETJsonArray() throws IOException {

        JSONArray jsonArray = null;
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();
        try{
            // Request setup
            this.httpURLConnection.setRequestMethod("GET");
            this.httpURLConnection.setConnectTimeout(5000);// 5000 milliseconds = 5 seconds
            this.httpURLConnection.setReadTimeout(5000);

            // Test if the response from the server is successful
            int status = this.httpURLConnection.getResponseCode();

            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(this.httpURLConnection.getErrorStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
            }
            else {
                reader = new BufferedReader(new InputStreamReader(this.httpURLConnection.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
            }
            jsonArray = new JSONArray(responseContent.toString());

        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            this.httpURLConnection.disconnect();
        }
        return jsonArray;
    }

    public void DeleteApiObject(int id) throws IOException {

        this.httpURLConnection.setRequestMethod("DELETE");

        this.httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
        this.httpURLConnection.setRequestProperty("Accept", "application/json");
        this.httpURLConnection.setRequestProperty("_token", "delete" + id);

        this.httpURLConnection.setDoOutput(true);

        int code = this.httpURLConnection.getResponseCode();
        System.out.println(code);

        try(BufferedReader br = new BufferedReader(new InputStreamReader(this.httpURLConnection.getInputStream(), "utf-8"))){
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }

    }

    public ArrayList<Coord> GETCoordArray() throws IOException {
        ArrayList<Coord> arrayList = new ArrayList<>();
        JSONArray jsonArray = GETJsonArray();
        for (int i = 0 ; i < jsonArray.length(); i++) {
            Coord apiObject = new Coord();
            apiObject.ParceJSON(jsonArray.getJSONObject(i));
            arrayList.add(apiObject);
        }
        return arrayList;
    }

    public ArrayList<Sensor> GETSensorArray() throws IOException {
        ArrayList<Sensor> arrayList = new ArrayList<>();
        JSONArray jsonArray = GETJsonArray();
        for (int i = 0 ; i < jsonArray.length(); i++) {
            Sensor apiObject = new Sensor();
            apiObject.ParceJSON(jsonArray.getJSONObject(i));
            arrayList.add(apiObject);
        }
        return arrayList;
    }

}
