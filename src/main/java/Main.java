import model.Coord;
import model.Fire;
import model.Sensor;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.util.UUID.randomUUID;

public class Main {
    private static final int NB_SENSORS = 60;

    static SimulatorManager manager = new SimulatorManager();
    static Coord beginCoords = new Coord(14, 21);

    public static void main(String[] args) throws InterruptedException {
        createSensors();
        manager.simulateFire(beginCoords);
    }

    public static void createSensors(){
        for(int i = 0; i < NB_SENSORS; i++){
            String id = String.valueOf(randomUUID());
            float x = -SimulatorManager.LIMIT_COORD + new Random().nextFloat() * (SimulatorManager.LIMIT_COORD + SimulatorManager.LIMIT_COORD);
            float y = -SimulatorManager.LIMIT_COORD + new Random().nextFloat() * (SimulatorManager.LIMIT_COORD + SimulatorManager.LIMIT_COORD);
            Sensor sensor = new Sensor(id, x, y);
            manager.sensors.add(sensor);
        }
        manager.displayList();
    }
}