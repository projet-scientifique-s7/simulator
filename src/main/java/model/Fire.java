package model;

import java.util.ArrayList;
import java.util.List;

import static java.util.UUID.randomUUID;

public class Fire {
    public List<Sensor> sensors = new ArrayList<Sensor>();
    String id;
    Sensor epicenter;

    public Fire(Sensor firstSensor){
        id = String.valueOf(randomUUID());
        sensors.add(firstSensor);
        epicenter = firstSensor;
    }

    public void turnOffFire() {
        for (int i = 0; i < sensors.size(); i++) {
            sensors.get(i).setState(0);
        }
    }

    public void turnOffFireGradually(Sensor sensor){
        //TODO: éteindre progressivement dans un thread ?
        sensor.setState(0);
    }
}
