package model;

import Utils.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Coord extends APIObject {
    //attributes
    public double x;
    public double y;
    public int z;


    public Coord(){

    }

    //constructor
    public Coord(double x, double y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Coord(double x, double y){
        this.x = x;
        this.y = y;
        this.z = 0;
    }


    public void ParceJSON( JSONObject jsonObject) {
        this.x = jsonObject.getDouble("x");
        this.y = jsonObject.getDouble("y");
        this.z = jsonObject.getInt("z");

    }

    public String getJSONString(){
        return "{\"x\": \"" + this.x + "\", \"y\": \"" + this.y + "\", \"z\":\"" + this.z + "\"}";
    }

    public String toString(){
        return "x:" + this.x +" ,y:" + this.y + " ,z:" + z;
    }
    
    //TODO Ajouter un JSON parser
}
