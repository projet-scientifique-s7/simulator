package ApiRequest;

import Utils.Request;
import model.Coord;
import model.Sensor;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import java.io.IOException;


public class ApiRequestTest {

    @Test
    public void GETSensorTest() throws IOException {
        ArrayList<Sensor> apiObjectArrayList = new Request("https://simulation.grp2.swano-lab.net/sensor").GETSensorArray();
        for (int i = 0; i<apiObjectArrayList.size(); i++){
            System.out.println(apiObjectArrayList.get(i).toString() + "\n");
        }
    }

    @Test
    public void POSTSensorTest() throws IOException {
        Request request = new Request("https://simulation.grp2.swano-lab.net/sensor/new");
        request.POSTApiObject(new Sensor("3", new Coord(45.77789, 4.855113, 5)));
    }

    @Test
    public void PUTSensorTest() throws IOException {
        ArrayList<Sensor> apiObjectArrayList = new Request("https://simulation.grp2.swano-lab.net/sensor").GETSensorArray();
        for (int i = 0; i<apiObjectArrayList.size(); i++){
            System.out.println(apiObjectArrayList.get(i).toString() + "\n");
        }
        apiObjectArrayList.get(0).setState(7);
        Request request = new Request("https://simulation.grp2.swano-lab.net/sensor/" + apiObjectArrayList.get(0).id + "/etat");
        request.PUTApiObject(apiObjectArrayList.get(0));

        ArrayList<Sensor> apiObjectArrayList2 = new Request("https://simulation.grp2.swano-lab.net/sensor").GETSensorArray();
        for (int i = 0; i<apiObjectArrayList2.size(); i++){
            System.out.println(apiObjectArrayList2.get(i).toString() + "\n");
        }
    }

    @Test
    public void DELETESensorTest() throws IOException {
        Request request = new Request("https://simulation.grp2.swano-lab.net/sensor/new");
        request.POSTApiObject(new Sensor(null, new Coord(45.77789, 4.855113, 5)));
        ArrayList<Sensor> apiObjectArrayList = new Request("https://simulation.grp2.swano-lab.net/sensor").GETSensorArray();
        for (int i = 0; i<apiObjectArrayList.size(); i++){
            System.out.println(apiObjectArrayList.get(i).toString() + "\n");
        }

        new Request("https://simulation.grp2.swano-lab.net/sensor/" + (apiObjectArrayList.size() - 1) ).DeleteApiObject((apiObjectArrayList.size() - 1));
        ArrayList<Sensor> apiObjectArrayList2 = new Request("https://simulation.grp2.swano-lab.net/sensor").GETSensorArray();
        for (int i = 0; i<apiObjectArrayList2.size(); i++){
            System.out.println(apiObjectArrayList2.get(i).toString() + "\n");
        }
    }
}
